#pragma once

#include <carry/detail/storage.hpp>

#include <wheels/core/assert.hpp>

#include <optional>

namespace carry {

//////////////////////////////////////////////////////////////////////

// Immutable mapping from string keys to arbitrary values

class Context {
  using StorageRef = detail::StorageRef;

 public:
  // Generic accessors

  template <typename TValue>
  std::optional<TValue> TryGet(const Key& key) const;

  template <typename TValue>
  TValue Get(const Key& key) const;

  template <typename TValue>
  TValue GetOr(const Key& key, TValue or_value) const;

  Keys CollectKeys(std::string_view prefix) const {
    return storage_->CollectKeys(prefix);
  }

  // Common types

  std::string GetString(const Key& key) const {
    return Get<std::string>(key);
  }

  int64_t GetInt64(const Key& key) const {
    return Get<int64_t>(key);
  }

  uint64_t GetUInt64(const Key& key) const {
    return Get<uint64_t>(key);
  }

  bool GetBool(const Key& key) const {
    return Get<bool>(key);
  }

  // For constructors
  static Context FromStorage(StorageRef storage) {
    return {std::move(storage)};
  }

  Context();

  // Private!
  StorageRef StoragePrivate() {
    return storage_;
  }

 private:
  Context(StorageRef storage)
      : storage_(std::move(storage)) {
  }

 private:
  StorageRef storage_;
};

}  // namespace carry

//////////////////////////////////////////////////////////////////////

#define CARRY_CONTEXT_IMPL
#include <carry/context-inl.hpp>
#undef CARRY_CONTEXT_IMPL
