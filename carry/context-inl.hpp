#ifndef CARRY_CONTEXT_IMPL
#error "Do not include this file directly, include carry/context.hpp instead
#endif

namespace carry {

template <typename TValue>
std::optional<TValue> Context::TryGet(const Key& key) const {
  auto any_value = storage_->Lookup(key);

  if (any_value.has_value()) {
    TValue* typed_value = std::any_cast<TValue>(&any_value);
    if (typed_value) {
      return *typed_value;
    } else {
      WHEELS_PANIC("Unexpected type for context key '" << key << "'");
    }
  }

  return std::nullopt;
}

template <typename TValue>
TValue Context::Get(const Key& key) const {
  auto maybe_value = TryGet<TValue>(key);
  WHEELS_VERIFY(maybe_value.has_value(), "Key " << key << " is missing");
  return *maybe_value;
}

template <typename TValue>
TValue Context::GetOr(const Key& key, TValue or_value) const {
  return TryGet<TValue>(key).value_or(or_value);
}

}  // namespace carry
